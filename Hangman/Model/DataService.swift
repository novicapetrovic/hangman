//
//  DataService.swift
//  Hangman
//
//  Created by Shiva Skanthan on 09/09/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//
//  Contains all the data and data related functions for categories and titles within a category

import Foundation

class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(name: category_moviesNshows, imageString: "movies_shows"),
        Category(name: category_actors, imageString: "actors"),
        Category(name: category_country, imageString: "country"),
        Category(name: category_capitals, imageString: "capitals")
    ]
    
    private let moviesNShowsArray = [
        
        //Movies
        SecretWordClass(title: "indiana jones"),
        SecretWordClass(title: "lord of the rings"),
        SecretWordClass(title: "the wolf of wall street"),
        SecretWordClass(title: "saving private ryan"),
        SecretWordClass(title: "shawshank redemption"),
        SecretWordClass(title: "the dark knight"),
        SecretWordClass(title: "the godfather"),
        SecretWordClass(title: "forrest gump"),
        SecretWordClass(title: "catch me if you can"),
        SecretWordClass(title: "the silence of the lambs"),
        SecretWordClass(title: "back to the future"),
        SecretWordClass(title: "django unchained"),
        SecretWordClass(title: "star wars"),
        SecretWordClass(title: "there will be blood"),
        SecretWordClass(title: "no country for old men"),
        SecretWordClass(title: "the hurt locker"),
        SecretWordClass(title: "the social network"),
        SecretWordClass(title: "wedding crashers"),
        SecretWordClass(title: "pirates of the caribbean"),
        SecretWordClass(title: "the da vinci code"),
        SecretWordClass(title: "mission impossible"),
        SecretWordClass(title: "blood diamond"),
        SecretWordClass(title: "the pursuit of happyness"),
        SecretWordClass(title: "night at the museum"),
        SecretWordClass(title: "slumdog millionaire"),
        SecretWordClass(title: "the curious case of benjamin button"),
        SecretWordClass(title: "pineapple express"),
        SecretWordClass(title: "role models"),
        SecretWordClass(title: "the hangover"),
        SecretWordClass(title: "law abiding citizen"),
        SecretWordClass(title: "shutter island"),
        SecretWordClass(title: "the kings speech"),
        SecretWordClass(title: "the other guys"),
        SecretWordClass(title: "planet of the apes"),
        SecretWordClass(title: "silver linings playbook"),
        SecretWordClass(title: "dallas buyers club"),
        SecretWordClass(title: "the conjuring"),
        SecretWordClass(title: "guardians of the galaxy"),
        SecretWordClass(title: "the imitation game"),
        SecretWordClass(title: "the theory of everything"),
        SecretWordClass(title: "the revenant"),
        SecretWordClass(title: "the hateful eight"),
        SecretWordClass(title: "bridge of spies"),
        SecretWordClass(title: "hacksaw ridge"),

        // Series
        SecretWordClass(title: "breaking bad"),
        SecretWordClass(title: "game of thrones"),
        SecretWordClass(title: "the walking dead"),
        SecretWordClass(title: "better call saul"),
        SecretWordClass(title: "homeland"),
        SecretWordClass(title: "stranger things"),
        SecretWordClass(title: "orange is the new black"),
        SecretWordClass(title: "house of cards"),
        SecretWordClass(title: "making a murderer"),
        SecretWordClass(title: "master of none"),
        SecretWordClass(title: "black mirror"),
        SecretWordClass(title: "peaky blinders"),
        SecretWordClass(title: "american horror story"),
        SecretWordClass(title: "sons of anarchy")
    ]
    
    private let countryArray = [
        // A (11 countries)
        SecretWordClass(title: "afghanistan"),
        SecretWordClass(title: "albania"),
        SecretWordClass(title: "algeria"),
        SecretWordClass(title: "andorra"),
        SecretWordClass(title: "angola"),
        SecretWordClass(title: "antigua and barbuda"),
        SecretWordClass(title: "argentina"),
        SecretWordClass(title: "armenia"),
        SecretWordClass(title: "australia"),
        SecretWordClass(title: "austria"),
        SecretWordClass(title: "azerbaijan"),
        
        // B (17 countries)
        SecretWordClass(title: "the bahamas"),
        SecretWordClass(title: "bahrain"),
        SecretWordClass(title: "bangladesh"),
        SecretWordClass(title: "barbados"),
        SecretWordClass(title: "belarus"),
        SecretWordClass(title: "belgium"),
        SecretWordClass(title: "belize"),
        SecretWordClass(title: "benin"),
        SecretWordClass(title: "bhutan"),
        SecretWordClass(title: "bolivia"),
        SecretWordClass(title: "bosnia and herzegovina"),
        SecretWordClass(title: "botswana"),
        SecretWordClass(title: "brazil"),
        SecretWordClass(title: "brunei"),
        SecretWordClass(title: "bulgaria"),
        SecretWordClass(title: "burkina faso"),
        SecretWordClass(title: "burundi"),
        
        // C (15 countries, excluding congo (2 versions, got confused) and cote d'ivoire)
        SecretWordClass(title: "cabo verde"),
        SecretWordClass(title: "cambodia"),
        SecretWordClass(title: "cameroon"),
        SecretWordClass(title: "canada"),
        SecretWordClass(title: "central african republic"),
        SecretWordClass(title: "chad"),
        SecretWordClass(title: "chile"),
        SecretWordClass(title: "china"),
        SecretWordClass(title: "colombia"),
        SecretWordClass(title: "comoros"),
        SecretWordClass(title: "costa rica"),
        SecretWordClass(title: "croatia"),
        SecretWordClass(title: "cuba"),
        SecretWordClass(title: "cyprus"),
        SecretWordClass(title: "czech republic"),
        
        // D (4 countries)
        SecretWordClass(title: "denmark"),
        SecretWordClass(title: "dijbouti"),
        SecretWordClass(title: "dominica"),
        SecretWordClass(title: "dominican republic"),
        
        // E (8 countries)
        SecretWordClass(title: "east timor"),
        SecretWordClass(title: "ecuador"),
        SecretWordClass(title: "egypt"),
        SecretWordClass(title: "el salvador"),
        SecretWordClass(title: "equatorial guinea"),
        SecretWordClass(title: "eritrea"),
        SecretWordClass(title: "estonia"),
        SecretWordClass(title: "ethiopia"),
        
        // F (3 countries)
        SecretWordClass(title: "fiji"),
        SecretWordClass(title: "finland"),
        SecretWordClass(title: "france"),
        
        // G (11 countries)
        SecretWordClass(title: "gambon"),
        SecretWordClass(title: "gambia"),
        SecretWordClass(title: "georgia"),
        SecretWordClass(title: "germany"),
        SecretWordClass(title: "ghana"),
        SecretWordClass(title: "greece"),
        SecretWordClass(title: "grenada"),
        SecretWordClass(title: "guatemala"),
        SecretWordClass(title: "guinea"),
        SecretWordClass(title: "guinea bissau"),
        SecretWordClass(title: "guvana"),
        
        // H (3 countries)
        SecretWordClass(title: "haiti"),
        SecretWordClass(title: "honduras"),
        SecretWordClass(title: "hungary"),
        
        // I (8 countries)
        SecretWordClass(title: "iceland"),
        SecretWordClass(title: "india"),
        SecretWordClass(title: "indonesia"),
        SecretWordClass(title: "iran"),
        SecretWordClass(title: "iraq"),
        SecretWordClass(title: "ireland"),
        SecretWordClass(title: "israel"),
        SecretWordClass(title: "italy"),
        
        // J (3 countries)
        SecretWordClass(title: "jamaica"),
        SecretWordClass(title: "japan"),
        SecretWordClass(title: "jordan"),
        
        // K (8 countries)
        SecretWordClass(title: "kazakhstan"),
        SecretWordClass(title: "kenya"),
        SecretWordClass(title: "kiribati"),
        SecretWordClass(title: "north korea"),
        SecretWordClass(title: "south korea"),
        SecretWordClass(title: "kosovo"),
        SecretWordClass(title: "kuwait"),
        SecretWordClass(title: "kyrgyzstan"),
        
        // L (9 countries)
        SecretWordClass(title: "laos"),
        SecretWordClass(title: "latvia"),
        SecretWordClass(title: "lebanon"),
        SecretWordClass(title: "lesotho"),
        SecretWordClass(title: "liberia"),
        SecretWordClass(title: "libya"),
        SecretWordClass(title: "liechtenstein"),
        SecretWordClass(title: "lithuania"),
        SecretWordClass(title: "luxembourg"),
        
        // M (19 countries)
        SecretWordClass(title: "macedonia"),
        SecretWordClass(title: "madagascar"),
        SecretWordClass(title: "malawi"),
        SecretWordClass(title: "malaysia"),
        SecretWordClass(title: "maldives"),
        SecretWordClass(title: "mali"),
        SecretWordClass(title: "malta"),
        SecretWordClass(title: "marshall islands"),
        SecretWordClass(title: "mauritania"),
        SecretWordClass(title: "mauritius"),
        SecretWordClass(title: "mexico"),
        SecretWordClass(title: "federate states of micronesia"),
        SecretWordClass(title: "moldova"),
        SecretWordClass(title: "monaco"),
        SecretWordClass(title: "mongolia"),
        SecretWordClass(title: "montenegro"),
        SecretWordClass(title: "morocco"),
        SecretWordClass(title: "mozambique"),
        SecretWordClass(title: "myanmar"),
        
        // N (9 countries)
        SecretWordClass(title: "namibia"),
        SecretWordClass(title: "nauru"),
        SecretWordClass(title: "nepal"),
        SecretWordClass(title: "netherlands"),
        SecretWordClass(title: "new zealand"),
        SecretWordClass(title: "nicaragua"),
        SecretWordClass(title: "niger"),
        SecretWordClass(title: "nigeria"),
        SecretWordClass(title: "norway"),
        
        // O (1 country)
        SecretWordClass(title: "oman"),
        
        // P (10 countries)
        SecretWordClass(title: "pakistan"),
        SecretWordClass(title: "palau"),
        SecretWordClass(title: "palestine"),
        SecretWordClass(title: "panama"),
        SecretWordClass(title: "papa new guinea"),
        SecretWordClass(title: "paraguay"),
        SecretWordClass(title: "peru"),
        SecretWordClass(title: "philippines"),
        SecretWordClass(title: "poland"),
        SecretWordClass(title: "portugal"),
        
        // Q (1 country)
        SecretWordClass(title: "qatar"),
        
        // R (3 countries)
        SecretWordClass(title: "romania"),
        SecretWordClass(title: "russia"),
        SecretWordClass(title: "rwanda"),
        
        // S (26 countries)
        SecretWordClass(title: "saint kitts and nevis"),
        SecretWordClass(title: "saint lucia"),
        SecretWordClass(title: "saint vincent and the grenadines"),
        SecretWordClass(title: "samoa"),
        SecretWordClass(title: "san marino"),
        SecretWordClass(title: "sao tome and principe"),
        SecretWordClass(title: "saudia arabia"),
        SecretWordClass(title: "senegal"),
        SecretWordClass(title: "serbia"),
        SecretWordClass(title: "seychelles"),
        SecretWordClass(title: "sierra leone"),
        SecretWordClass(title: "singapore"),
        SecretWordClass(title: "slovakia"),
        SecretWordClass(title: "slovenia"),
        SecretWordClass(title: "solomon islands"),
        SecretWordClass(title: "somalia"),
        SecretWordClass(title: "south africa"),
        SecretWordClass(title: "spain"),
        SecretWordClass(title: "sri lanka"),
        SecretWordClass(title: "sudan"),
        SecretWordClass(title: "south sudan"),
        SecretWordClass(title: "suriname"),
        SecretWordClass(title: "swaziland"),
        SecretWordClass(title: "sweden"),
        SecretWordClass(title: "switzerland"),
        SecretWordClass(title: "syria"),
        
        // T (11 countries)
        SecretWordClass(title: "taiwan"),
        SecretWordClass(title: "tajikistan"),
        SecretWordClass(title: "tanzania"),
        SecretWordClass(title: "thailand"),
        SecretWordClass(title: "togo"),
        SecretWordClass(title: "tonga"),
        SecretWordClass(title: "trinidad and tobago"),
        SecretWordClass(title: "tunisia"),
        SecretWordClass(title: "turkey"),
        SecretWordClass(title: "turkmenistan"),
        SecretWordClass(title: "tuvalu"),
        
        // U (7 countries)
        SecretWordClass(title: "uganda"),
        SecretWordClass(title: "ukraine"),
        SecretWordClass(title: "united arab emirates"),
        SecretWordClass(title: "united kingdom"),
        SecretWordClass(title: "united states"),
        SecretWordClass(title: "uruguay"),
        SecretWordClass(title: "uzbekistan"),
        
        // V (4 countries)
        SecretWordClass(title: "vanuatu"),
        SecretWordClass(title: "vatican city"),
        SecretWordClass(title: "venezuela"),
        SecretWordClass(title: "vietnam"),
        
        // Y (1 country)
        SecretWordClass(title: "yemen"),
        
        // Z (
        SecretWordClass(title: "zambia"),
        SecretWordClass(title: "zimbabwe")
    ]
    
    private let capitalsArray = [
        // A (11 countries)
        SecretWordClass(title: "kabul"),    // afghanistan
        SecretWordClass(title: "tirana"),   // albania
        SecretWordClass(title: "algiers"),  // algeria
        SecretWordClass(title: "andorra la vella"), // andorra
        SecretWordClass(title: "luanda"),   // angola
        SecretWordClass(title: "saint johns"),  // antigua and barbuda
        SecretWordClass(title: "buenos aires"), // argentina
        SecretWordClass(title: "yerevan"),  // armenia
        SecretWordClass(title: "canberra"),    // australia
        SecretWordClass(title: "vienna"),  // austria
        SecretWordClass(title: "baku"),   // azerbaijan
        
        // B (17 countries)
        SecretWordClass(title: "nassau"),  // the bahamas
        SecretWordClass(title: "manama"),  // bahrain
        SecretWordClass(title: "dhaka"),   // bangladesh
        SecretWordClass(title: "bridgetown"), // barbados
        SecretWordClass(title: "minsk"),  // belarus
        SecretWordClass(title: "brussels"),  // belgium
        SecretWordClass(title: "belmopan"),   // belize
        SecretWordClass(title: "porto novo"), // benin
        SecretWordClass(title: "thimphu"),   // bhutan
        SecretWordClass(title: "sucre la paz"),  // bolivia
        SecretWordClass(title: "sarajevo"),   // bosnia and herzegovina
        SecretWordClass(title: "gaborone"), // botswana
        SecretWordClass(title: "brasilia"),   // brazil
        SecretWordClass(title: "bandar seri begawan"),   // brunei
        SecretWordClass(title: "sofia"), // bulgaria
        SecretWordClass(title: "ouagadougou"), // burkina faso
        SecretWordClass(title: "bujumbura"),  // burundi
        
        // C (15 countries, excluding congo (2 versions, got confused) and cote d'ivoire)
        SecretWordClass(title: "praia"),   // cabo verde
        SecretWordClass(title: "phnom penh"), // cambodia
        SecretWordClass(title: "yaounde"), // cameroon
        SecretWordClass(title: "ottawa"),   // canada
        SecretWordClass(title: "bangui"), // central african republic
        SecretWordClass(title: "ndjamena"), // chad
        SecretWordClass(title: "santiago"),    // chile
        SecretWordClass(title: "beijing"),    // china
        SecretWordClass(title: "bogota"), // colombia
        SecretWordClass(title: "moroni"),  // comoros
        SecretWordClass(title: "san jose"),   // costa rica
        SecretWordClass(title: "zagreb"),  // croatia
        SecretWordClass(title: "havana"), // cuba
        SecretWordClass(title: "nicosia"),   // cyprus
        SecretWordClass(title: "prague"),   // czech republic
        
        // D (4 countries)
        SecretWordClass(title: "copenhagen"),  // denmark
        SecretWordClass(title: "dijbouti city"), // dijbouti
        SecretWordClass(title: "roseau"), // dominica
        SecretWordClass(title: "santo domingo"),   // dominican republic
        
        // E (7 countries) omitted: east timor
        SecretWordClass(title: "quito"),  // ecuador
        SecretWordClass(title: "cairo"),    // egypt
        SecretWordClass(title: "san salvador"),  // el salvador
        SecretWordClass(title: "malabo oyala"),    // equatorial guinea
        SecretWordClass(title: "asmara"),  // eritrea
        SecretWordClass(title: "tallinn"),  // estonia
        SecretWordClass(title: "addis adaba"), // ethiopia
        
        // F (3 countries)
        SecretWordClass(title: "suva"), // fiji
        SecretWordClass(title: "helsinki"),  // finland
        SecretWordClass(title: "paris"),   // france
        
        // G (11 countries)
        SecretWordClass(title: "libreville"),   // gambon
        SecretWordClass(title: "banjul"),   // gambia
        SecretWordClass(title: "tbilsi"),  // georgia
        SecretWordClass(title: "berlin"),  // germany
        SecretWordClass(title: "accra"),    // ghana
        SecretWordClass(title: "athens"),   // greece
        SecretWordClass(title: "saint georges"),  // grenada
        SecretWordClass(title: "guatemala city"),    // guatemala
        SecretWordClass(title: "conakry"),   // guinea
        SecretWordClass(title: "bissau"),    // guinea bissau
        SecretWordClass(title: "georgetown"),   // guvana
        
        // H (3 countries)
        SecretWordClass(title: "port au prince"),    // haiti
        SecretWordClass(title: "tegucigalpa"), // honduras
        SecretWordClass(title: "budapest"),  // hungary
        
        // I (8 countries)
        SecretWordClass(title: "reykjavik"),  // iceland
        SecretWordClass(title: "new delhi"),    // india
        SecretWordClass(title: "jakarta"),    // indonesia
        SecretWordClass(title: "tehran"), // iran
        SecretWordClass(title: "baghdad"), // iraq
        SecretWordClass(title: "dublin"),  // ireland
        SecretWordClass(title: "jerusalem"),   // isreal
        SecretWordClass(title: "rome"),    // italy
        
        // J (3 countries)
        SecretWordClass(title: "kingston"),  // jamaica
        SecretWordClass(title: "tokyo"),    // japan
        SecretWordClass(title: "amman"),   // jordan
        
        // K (8 countries)
        SecretWordClass(title: "astana"),   // kazakhstan
        SecretWordClass(title: "nairobi"),    // kenya
        SecretWordClass(title: "tarawa"), // kiribati
        SecretWordClass(title: "pyongyang"),  // north korea
        SecretWordClass(title: "seoul"),  // south korea
        SecretWordClass(title: "pristina"),   // kosovo
        SecretWordClass(title: "kuwait city"),   // kuwait
        SecretWordClass(title: "bishkek"),   // kyrgyzstan
        
        // L (9 countries)
        SecretWordClass(title: "vientiane"), // laos
        SecretWordClass(title: "riga"),   // latvia
        SecretWordClass(title: "beirut"),  // lebanon
        SecretWordClass(title: "maseru"),  // lesotho
        SecretWordClass(title: "monrovia"),  // liberia
        SecretWordClass(title: "tripoli"),    // libya
        SecretWordClass(title: "vaduz"),    // liechtenstein
        SecretWordClass(title: "vilnius"),    // lithuania
        SecretWordClass(title: "luxembourg city"),   // luxembourg
        
        // M (19 countries)
        SecretWordClass(title: "skopje"),    // macedonia
        SecretWordClass(title: "antananarivo"),   // madagascar
        SecretWordClass(title: "lilongwe"),   // malawi
        SecretWordClass(title: "kuala lumpur"), // malaysia
        SecretWordClass(title: "male"), // maldives
        SecretWordClass(title: "bamako"), // mali
        SecretWordClass(title: "valletta"),    // malta
        SecretWordClass(title: "majuro"), // marshall islands
        SecretWordClass(title: "nouakchott"),   // mauritania
        SecretWordClass(title: "port louis"),    // mauritius
        SecretWordClass(title: "mexico city"),   // mexico
        SecretWordClass(title: "palikir"),    // federate states of micronesia
        SecretWordClass(title: "chisinau"),  // moldova
        SecretWordClass(title: "monaco"),   // monaco
        SecretWordClass(title: "ulaanbaatar"), // mongolia
        SecretWordClass(title: "podgorica"),   // montenegro
        SecretWordClass(title: "rabat"),  // morocco
        SecretWordClass(title: "maputo"),   // mozambique
        SecretWordClass(title: "naypyidaw"),  // myanmar
        
        // N (9 countries)
        SecretWordClass(title: "windhoek"),  // namibia
        SecretWordClass(title: "yaren district"),    // nauru
        SecretWordClass(title: "kathmandu"),    // nepal
        SecretWordClass(title: "amsterdam"),  // netherlands
        SecretWordClass(title: "wellington"),  // new zealand
        SecretWordClass(title: "managua"),    // nicaragua
        SecretWordClass(title: "niamey"),    // niger
        SecretWordClass(title: "abuja"),  // nigeria
        SecretWordClass(title: "oslo"),   // norway
        
        // O (1 country)
        SecretWordClass(title: "muscat"), // oman
        
        // P (10 countries)
        SecretWordClass(title: "islamabad"), // pakistan
        SecretWordClass(title: "ngerulmud"),    // palau
        SecretWordClass(title: "jerusalem"),    // palestine
        SecretWordClass(title: "panama city"),   // panama
        SecretWordClass(title: "port moresby"),  // papa new guinea
        SecretWordClass(title: "asuncion"), // paraguay
        SecretWordClass(title: "lima"), // peru
        SecretWordClass(title: "manila"),  // philippines
        SecretWordClass(title: "warsaw"),   // poland
        SecretWordClass(title: "lisbon"), // portugal
        
        // Q (1 country)
        SecretWordClass(title: "doha"),    // qatar
        
        // R (3 countries)
        SecretWordClass(title: "bucharest"),  // romania
        SecretWordClass(title: "moscow"),   // russia
        SecretWordClass(title: "kigali"),   // rwanda
        
        // S (25 countries) omitted: swaziland
        SecretWordClass(title: "basseterre"),    // saint kitts and nevis
        SecretWordClass(title: "castries"),  // saint lucia
        SecretWordClass(title: "kingston"), // saint vincent and the grenadines
        SecretWordClass(title: "apia"),    // samoa
        SecretWordClass(title: "san marino"),   // san marino
        SecretWordClass(title: "sao tome"),    // sao tome and principe
        SecretWordClass(title: "riyadh"),    // saudia arabia
        SecretWordClass(title: "dakar"),  // senegal
        SecretWordClass(title: "belgrade"),   // serbia
        SecretWordClass(title: "victoria"),   // seychelles
        SecretWordClass(title: "freetown"), // sierra leone
        SecretWordClass(title: "singapore"),    // singapore
        SecretWordClass(title: "bratislava"), // slovakia
        SecretWordClass(title: "ljubjana"), // slovenia
        SecretWordClass(title: "honiara"),  // solomon islands
        SecretWordClass(title: "mogadishu"),  // somalia
        SecretWordClass(title: "pretoria"), // south africa ** EXCEPTIONAL CASE
        SecretWordClass(title: "madrid"),    // spain
        SecretWordClass(title: "sri jayawardenpura kotte"),    // sri lanka
        SecretWordClass(title: "khartoum"),    // sudan
        SecretWordClass(title: "khartoum"),  // south sudan
        SecretWordClass(title: "paramaribo"), // suriname
        SecretWordClass(title: "stockholm"),   // sweden
        SecretWordClass(title: "bern"),  // switzerland
        SecretWordClass(title: "damascus"),    // syria
        
        // T (11 countries)
        SecretWordClass(title: "taipei"),   // taiwan
        SecretWordClass(title: "dushanbe"),   // tajikistan
        SecretWordClass(title: "dodoma"), // tanzania
        SecretWordClass(title: "bangkok"), // thailand
        SecretWordClass(title: "lome"), // togo
        SecretWordClass(title: "nukualofa"),    // tonga
        SecretWordClass(title: "port of spain"),  // trinidad and tobago
        SecretWordClass(title: "tunis"),  // tunisia
        SecretWordClass(title: "ankara"),   // turkey
        SecretWordClass(title: "ashgabat"), // turkmenistan
        SecretWordClass(title: "funafuti"),   // tuvalu
        
        // U (7 countries)
        SecretWordClass(title: "kampala"),   // uganda
        SecretWordClass(title: "kyiv"),  // ukraine
        SecretWordClass(title: "abu dhabi"), // united arab emirates
        SecretWordClass(title: "london"),   // united kingdom
        SecretWordClass(title: "washington dc"),    // united states
        SecretWordClass(title: "montevideo"),  // uruguay
        SecretWordClass(title: "tashkent"),   // uzbekistan
        
        // V (4 countries)
        SecretWordClass(title: "port vila"),  // vanuatu
        SecretWordClass(title: "vatican city"), // vatican city
        SecretWordClass(title: "caracas"),    // venezuela
        SecretWordClass(title: "hanoi"),  // vietnam
        
        // Y (1 country)
        SecretWordClass(title: "sanaa"),    // yemen
        
        // Z (
        SecretWordClass(title: "lusaka"),   // zambia
        SecretWordClass(title: "harare")  // zimbabwe
    ]
    
    private let actorsArray = [

        SecretWordClass(title: "robert de niro"),
        SecretWordClass(title: "jack nicholson"),
        SecretWordClass(title: "tom hanks"),
        SecretWordClass(title: "leonardo dicaprio"),
        SecretWordClass(title: "johnny depp"),
        SecretWordClass(title: "al pacino"),
        SecretWordClass(title: "denzel washington"),
        SecretWordClass(title: "brad pitt"),
        SecretWordClass(title: "daniel day lewis"),
        SecretWordClass(title: "tom cruise"),
        SecretWordClass(title: "dustin hoffman"),
        SecretWordClass(title: "sean penn"),
        SecretWordClass(title: "christian bale"),
        SecretWordClass(title: "harrison ford"),
        SecretWordClass(title: "george clooney"),
        SecretWordClass(title: "russell crowe"),
        SecretWordClass(title: "meryl streep"),
        SecretWordClass(title: "jennifer lawrence"),
        SecretWordClass(title: "kate winslet"),
        SecretWordClass(title: "cate blanchett"),
        SecretWordClass(title: "sandra bullock"),
        SecretWordClass(title: "natalie portman"),
        SecretWordClass(title: "julia roberts"),
        SecretWordClass(title: "matt damon"),
        SecretWordClass(title: "robin williams"),
        SecretWordClass(title: "christoph waltz"),
        SecretWordClass(title: "heath ledger"),
        SecretWordClass(title: "liam neeson"),
        SecretWordClass(title: "bruce willis"),
        SecretWordClass(title: "philip seymour hoffman"),
        SecretWordClass(title: "will smith"),
        SecretWordClass(title: "angelina jolie"),
        SecretWordClass(title: "anne hathaway"),
        ]
    
    //MARK: Get all categories
    func getAllCategories() -> [Category] {
        return categories
    }
    
    //MARK: Get all titles for a category
    func getAllTitles(forCategory title: String) -> [SecretWordClass] {
        switch title {
        case category_moviesNshows:
            return getMoviesNShows()
        case category_actors:
            return getActors()
        case category_country:
            return getCountry()
        case category_capitals:
            return getCapitals()
        default:
            return getMoviesNShows()
        }
    }
    
    func getMoviesNShows() -> [SecretWordClass] {
      return moviesNShowsArray
    }
    
    func getCountry() -> [SecretWordClass] {
        return countryArray
    }
    
    func getActors() -> [SecretWordClass] {
        return actorsArray
    }
    
    func getCapitals() -> [SecretWordClass] {
        return capitalsArray
    }
}
