//
//  Category.swift
//  Hangman
//
//  Created by Shiva Skanthan on 09/09/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//
//  The Category class defines the properties of category

import Foundation

class Category {
    
    var name: String
    var imageString: String
    
    init(name: String, imageString: String) {
        self.name = name
        self.imageString = imageString
    }
}
