//
//  SecretWordClass.swift
//  Hangman
//
//  Created by Novica Petrovic on 19/08/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//
//  The SecretWordClass is the data structure for the secret words. The data structure contains 2 things, the secret word itself as a string, and a dictionary with all of it's unique elements as keys and an array of integers containing it's position in the secret word.

import Foundation

class SecretWordClass {
    
    var secretWord : String
    var indexDictionary : [Character : NSMutableArray]
    
    init(title: String) {
        secretWord = title
        func createDictionary(anything: String) -> [Character : NSMutableArray] {
            var myDict : [Character : NSMutableArray] = [:]
            var index = 0
            for letter in title {
                if myDict[letter] == nil {
                    myDict[letter] = [index]
                } else {
                    myDict[letter]?.add(index)
                }
                index += 1
            }
            return myDict
        }
        indexDictionary = createDictionary(anything: secretWord)
    }
    
}
