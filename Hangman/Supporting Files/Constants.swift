//
//  Constants.swift
//  Hangman
//
//  Created by Shiva Skanthan on 17/09/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import Foundation

//MARK:- Category types
public let category_moviesNshows = "Movies & Shows"
public let category_country = "Countries"
public let category_actors = "Actors"
public let category_capitals = "Capitals"

//MARK:- Segue Identifiers
public let goToHomeVC = "goToHomeVC"
public let goToGameVC = "goToGameVC"
public let goToSettingsVC = "goToSettingsVC"

//MARK:- Images
public let hangman0 = "hangman0"
public let hangman1 = "hangman1"
public let hangman2 = "hangman2"
public let hangman3 = "hangman3"
public let hangman4 = "hangman4"
public let hangman5 = "hangman5"
public let hangman6 = "hangman6"

//MARK:- Sounds
public let correctTile = "Creepy-Roll-Over-2"
public let wrongTile = "Creepy_Percussion_8"
public let gameOverSound = "Jigsaw Laugh 2017"
public let correctAnswerSound = "Correct Answer Sound Effect"

//MARK:- Background Music
public let inGameMusic = "The-Island-of-Dr-Sinister"

//MARK:- Extension
public let mp3 = "mp3"

//MARK:- Settings
public let soundEffectSetting = "soundEffectSetting"
public let backgroundMusicSetting = "backgroundMusicSetting"
