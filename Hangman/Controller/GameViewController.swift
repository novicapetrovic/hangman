//
//  ViewController.swift
//  Hangman
//
//  Created by Novica Petrovic on 19/08/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit
import AVFoundation
import SCLAlertView
import SVProgressHUD

class GameViewController: UIViewController {
    
    //MARK:- Variable and Constant
    var category: Category!
    var categoryTitlesArray: [SecretWordClass]!
    var currentTitle: SecretWordClass!
    
    var timer = Timer()
    var audioPlayer: AVAudioPlayer!
    
    var numberOfLives: Int = 3
    var guessesLeft: Int = 5
    var winStreak: Int = 0
    var timeLeft: Double = 60
    
    //TODO:- Check if user has enabled in-game sound effects
    let soundEffectsEnabled = UserDefaults.standard.bool(forKey: soundEffectSetting)
    let imagesArray = [hangman0, hangman1, hangman2, hangman3, hangman4, hangman5, hangman6]
    
    //MARK:- IBOutlet
    @IBOutlet weak var secretWordLabel: UILabel!
    @IBOutlet weak var hangmanImage: UIImageView!
    @IBOutlet weak var numberOfLivesLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var winStreakLabel: UILabel!
    @IBOutlet weak var keyboardOutlet: UIStackView!
    @IBOutlet var alphabetsButton: [UIButton]!
    @IBOutlet weak var navigationHeader: UILabel!
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Update navigation header and start game
        if let category = category?.name {
            navigationHeader.text = category
            startGame(forCategory: category)
        }
    }
    
    //MARK:- IBAction
    
    //MARK: Letter pressed
    //There are 2 scenarios that need to be considered within the letterPressed function
    // 1) the user guesses the letter correctly
    // Within this scenario, there are 2 scenarios that need to be considered
    // 1a) the user has guessed the entire secret word
    // 1b) the user has not yet guessed the entire secret word
    
    // 2) the user guesses the letter incorrectly
    // Within this scenario, there are 4 scenarios that need to be considered
    // 2a) the user has run out of guesses and lives
    // 2b) the user has run out of guesses but not lives
    // 2c) the user has not run out of guesses
    
    
    @IBAction func letterPressed(_ sender: UIButton) {
        
        let letterGuessed : Character = Character((sender.titleLabel?.text!)!)
        
        // Scenario 1: User guesses the letter correctly
        if currentTitle.indexDictionary[letterGuessed] != nil {
            
            for index in currentTitle.indexDictionary[letterGuessed]! {
                let secretWord = replace(myString: secretWordLabel.text!, index as! Int, letterGuessed)
                updateUI(secretWord: secretWord, numberOfLives: nil, timeLeft: nil, winStreak: nil)
            }
            
            // Scenario 1a: the user has guessed the entire secret word
            if secretWordLabel.text == currentTitle.secretWord {
                self.keyboardOutlet.isUserInteractionEnabled = false
                restartForWin()
            } else { // Scenario 1b: the user has not yet guessed the entire word
                playSound(playSound: correctTile)
                sender.backgroundColor = UIColor.green
                sender.isEnabled = false
            }
            
        } else { // Scenario 2: User guesses the letter incorrectly
            guessesLeft -= 1
            sender.backgroundColor = UIColor.red
            sender.isEnabled = false
            hangmanImage.image = UIImage(named: imagesArray[guessesLeft])
            playSound(playSound: wrongTile)
            
            if guessesLeft == 0 {
                self.keyboardOutlet.isUserInteractionEnabled = false
                numberOfLives -= 1
                // Scenario 2a) the user has run out of guesses and lives
                if numberOfLives == 0 {
                    gameOver()
                } else {
                    restartForLoss()
                }
            }
        }
    }
    
    //MARK: Restart game
    @IBAction func restartButtonPressed(_ sender: UIButton) {
        
        //MARK: Instatiate a new alert view
        let alertView = createNewAlert()
        
        //MARK: Add button to alert
        alertView.addButton("Restart") {
            self.restartGame()
        }
        
        //MARK: Display alert
        alertView.showWarning("Restart game", subTitle: "Are you sure you want to restart the game?")
        
    }
    
    //MARK:- End game
    @IBAction func backButtonPressed(_ sender: Any) {
        
        self.timer.invalidate()
        
        //MARK: Instatiate a new alert view
        let alertView = createNewAlert()
        
        //MARK: Add button to alert
        alertView.addButton("Leave") {
            self.dismiss(animated: true, completion: nil)
        }
        
        //MARK: Display alert
        alertView.showError("Leave game", subTitle: "The game ends when you leave.")
        
    }
    //MARK:- Function
    
    //MARK: Start game
    func startGame(forCategory category: String) {
        
        //MARK: Create an array with all category titles
        categoryTitlesArray = DataService.instance.getAllTitles(forCategory: category)
        
        //MARK: Select a random title from the array
        let currentTitleSecretWord = newSecretWord()
        
        //Mark: Update UI
        updateUI(secretWord: currentTitleSecretWord, numberOfLives: numberOfLives, timeLeft: timeLeft, winStreak: winStreak)
        
        //MARK: Start timer
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(countdownTimer), userInfo: nil, repeats: true)
    }
    
    //MARK:Update UI
    func updateUI(secretWord: String?, numberOfLives: Int?, timeLeft: Double?, winStreak: Int?) {
        if let secretWord = secretWord {
            secretWordLabel.text = secretWord
        }
        if let numberOfLives = numberOfLives {
            numberOfLivesLabel.text = String(numberOfLives)
        }
        if let timeLeft = timeLeft {
            timeLabel.text = String(timeLeft)
        }
        if let winStreak = winStreak {
            winStreakLabel.text = String(winStreak)
        }
    }
    
    //MARK: Generates a new title from the category array
    func newSecretWord() -> String {
        
        //MARK: Create a random integer
        let randomIndex = createARandomInteger()
        
        //MARK: Extract title in category array
        currentTitle = categoryTitlesArray[randomIndex]
        
        //MARK: Create secret word from title
        let newTitleSecretWord = createSecretTitle(forTitle: currentTitle.secretWord)
        
        //MARK: Removes title from category array
        categoryTitlesArray.remove(at: randomIndex)
        
        print(currentTitle.secretWord)
        
        return newTitleSecretWord
    }
    
    //MARK: Create a random index
    func createARandomInteger() -> Int {
        
        //MARK: Reinitialises category array if it is empty
        if categoryTitlesArray.isEmpty {
            categoryTitlesArray = DataService.instance.getAllTitles(forCategory: (category!.name))
        }
        
        //MARK: Defines a random number from the array
        let randomIndex = Int.random(in: 0...categoryTitlesArray.count-1)
        
        return randomIndex
    }
    
    //MARK: Setup a pop up alert
    func createNewAlert() -> SCLAlertView {
        //MARK: Modify appearence of alert
        let appearence = SCLAlertView.SCLAppearance(showCloseButton: false, hideWhenBackgroundViewIsTapped: true)
        
        //MARK: Add new appeareance to alert
        let alertView = SCLAlertView(appearance: appearence)
        
        return alertView
    }
    
    //MARK: Convert title to secret
    func createSecretTitle(forTitle title: String) -> String {
        
        var secretTitle = ""
        
        for letter in title {
            if letter == " " {
                secretTitle.append(" ")
            } else {
                secretTitle.append("-")
            }
        }
        return secretTitle
    }
    
    //MARK: Replace
    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString)     // gets an array of characters
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }
    
    //MARK: Restart game
    func restartGame() {
        
        for buttons in alphabetsButton {
            buttons.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            buttons.isEnabled = true
        }
        
        timer.invalidate()
        timeLeft = 60
        numberOfLives = 3
        guessesLeft = 5
        winStreak = 0
        
        hangmanImage.image = nil
        keyboardOutlet.isUserInteractionEnabled = true
        
        //START GAME
        startGame(forCategory: category.name)
    }
    
    //MARK: Restart for win
    func restartForWin() {
        
        //MARK: Adds a dark themed progress hud when user gets the correct answer
        playSound(playSound: correctAnswerSound)
        self.timer.invalidate()
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.showSuccess(withStatus: "Well done!")
        SVProgressHUD.dismiss(withDelay: 0.7) {
            
            //MARK: Resets button colors to white
            for buttons in self.alphabetsButton {
                buttons.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                buttons.isEnabled = true
            }
            
            //MARK: Update variables
            self.winStreak += 1
            self.timeLeft = Double(60 - self.winStreak)
            self.guessesLeft = 5
            self.hangmanImage.image = nil
            
            //MARK: Enable keyboard
            self.keyboardOutlet.isUserInteractionEnabled = true
            
            //MARK: Select a random title from the array
            let currentTitleSecretWord = self.newSecretWord()
            
            //Mark: Update UI
            self.updateUI(secretWord: currentTitleSecretWord, numberOfLives: self.numberOfLives, timeLeft: self.timeLeft, winStreak: self.winStreak)
            
            //MARK: Start timer
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.countdownTimer), userInfo: nil, repeats: true)
        }
    }
    
    //MARK: Restart for loss
    func restartForLoss() {
        
        //MARK: Adds a dark themed progress hud when user gets the wrong answer
        secretWordLabel.text = currentTitle.secretWord
        playSound(playSound: wrongTile)
        self.timer.invalidate()
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.showError(withStatus: "unlucky...")
        SVProgressHUD.dismiss(withDelay: 2) {
            
            //MARK: Resets button colors to white
            for buttons in self.alphabetsButton {
                buttons.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                buttons.isEnabled = true
            }
            
            //MARK: Update variables
            self.timeLeft = Double(60 - self.winStreak)
            self.hangmanImage.image = nil
            self.guessesLeft = 5
            
            //MARK: Enable keyboard
            self.keyboardOutlet.isUserInteractionEnabled = true
            
            //MARK: Select a random title from the array
            let currentTitleSecretWord = self.newSecretWord()
            
            //Mark: Update UI
            self.updateUI(secretWord: currentTitleSecretWord, numberOfLives: self.numberOfLives, timeLeft: self.timeLeft, winStreak: self.winStreak)
            
            //MARK: Start timer
            self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.countdownTimer), userInfo: nil, repeats: true)
        }
    }
    
    func gameOver() {
        
        //MARK: Game Over
        timer.invalidate()
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.showError(withStatus: "Game Over!")
        SVProgressHUD.dismiss(withDelay: 2)
        
        keyboardOutlet.isUserInteractionEnabled = false
        playSound(playSound: gameOverSound)
        hangmanImage.image = UIImage(named: imagesArray[0])
        updateUI(secretWord: currentTitle.secretWord, numberOfLives: 0, timeLeft: nil, winStreak: nil)
    }
    
    //MARK: Play sound
    func playSound(playSound: String) {
        
        if soundEffectsEnabled {
            let soundURL = Bundle.main.url(forResource: playSound, withExtension: mp3)
            
            do{
                audioPlayer = try AVAudioPlayer(contentsOf: soundURL!)
                audioPlayer.play()
            }
            catch {
                print(error)
            }
        } else {
            return
        }
        
    }
    
    //MARK: Action
    @objc func countdownTimer() {
        
        timeLeft -= 0.1
        
        //MARK: Time is rounded off to ensure final value is 0.0 seconds
        let roundedTime = Double(round(10*timeLeft)/10)
        //MARK: Update time label
        updateUI(secretWord: nil, numberOfLives: nil, timeLeft: roundedTime, winStreak: nil)
        //MARK: Time is up
        if roundedTime == 0.0 {
            numberOfLives -= 1
            
            if numberOfLives == 0 {
                gameOver()
            } else {
                restartForLoss()
            }
        }
    }
    
    // Set status bar to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
