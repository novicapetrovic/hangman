//
//  HomeViewController.swift
//  Hangman
//
//  Created by Shiva Skanthan on 06/09/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit
import AVFoundation

class HomeViewController: UIViewController, UITableViewDelegate {

    //MARK:- Variable and Constant
    var allCategories: [Category]!
    
    //MARK:- IBOutlet
    @IBOutlet weak var categoryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Starts background music if setting is set to ON
        if UserDefaults.standard.bool(forKey: backgroundMusicSetting) {
            backgroundMusic?.play()
        }
        
        //MARK: Check for changes in background music setting
        NotificationCenter.default.addObserver(self, selector: #selector(toggleMusic), name: Notification.Name(backgroundMusicSetting), object: nil)
        
        //TODO:- Set table view as delegate and datasource
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        
        //TODO:- Retrieve all categories from Data Service
        allCategories = DataService.instance.getAllCategories()
    }
    
    //MARK: Turns audio music on or off based on settings
    @objc func toggleMusic(note: Notification) {
        
        let backgroundMusicIsOn = UserDefaults.standard.bool(forKey: backgroundMusicSetting)
        
        if backgroundMusicIsOn {
            backgroundMusic?.play()
        } else {
            backgroundMusic?.stop()
        }
        
    }
    
    //MARK: Set up audio player
    let backgroundMusic: AVAudioPlayer? = {
        guard let soundURL = Bundle.main.url(forResource: inGameMusic, withExtension: mp3) else {
            return nil
        }
        do {
            let player = try AVAudioPlayer(contentsOf: soundURL)
            player.numberOfLoops = -1
            return player
        } catch {
            return nil
        }
    }()
    
}



extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //TODO:- Total category count
        return allCategories!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //TODO:- Create a cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryTableViewCell
        
        let categoryImageString = allCategories[indexPath.row].imageString
        
        //TODO:- Add property to cell
        cell.categoryName.text = allCategories[indexPath.row].name
        cell.categoryImage.image = UIImage(named: categoryImageString)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        //TODO:- Instantiates the GameVC
        let gameVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "gameVC") as! GameViewController
        //TODO:- Sends selected category to GameVC
        gameVC.category = allCategories[indexPath.row]
        //TODO:- Presents GameVC
        present(gameVC, animated: true, completion: nil)
    }
    
    // Set status bar to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
