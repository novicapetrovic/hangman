//
//  WelcomeViewController.swift
//  Hangman
//
//  Created by Shiva Skanthan on 07/10/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit
import AVFoundation

class WelcomeViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imageViewBackground: UIView!
    @IBOutlet weak var tapToPlayButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Starts background music if setting is set to ON
        if UserDefaults.standard.bool(forKey: backgroundMusicSetting) {
            backgroundMusic?.play()
        }
        
        //MARK: Allow user to segue to HomeVC by tapping anywhere on the screen
        let tapGestureRecogniser = UITapGestureRecognizer()
        tapGestureRecogniser.addTarget(self, action: #selector (segueToHomeScreenVC))
        imageViewBackground.addGestureRecognizer(tapGestureRecogniser)
        
        //MARK: Check for changes in background music setting
        NotificationCenter.default.addObserver(self, selector: #selector(toggleMusic), name: Notification.Name(backgroundMusicSetting), object: nil)
        
    }
    
    //MARK:- IBAction
    
    @IBAction func tapToPlayButtonPressed(_ sender: Any) {
        segueToHomeScreenVC()
    }
    //MARK:- Functions
    
    //MARK: Segue to home screen
    @objc func segueToHomeScreenVC() {
        performSegue(withIdentifier: goToHomeVC, sender: self)
    }
    
    //MARK: Turns audio music on or off based on settings
    @objc func toggleMusic(note: Notification) {
        
        let backgroundMusicIsOn = UserDefaults.standard.bool(forKey: backgroundMusicSetting)
        
        if backgroundMusicIsOn {
            backgroundMusic?.play()
        } else {
            backgroundMusic?.stop()
        }
        
    }
    
    //MARK: Set up audio player
    let backgroundMusic: AVAudioPlayer? = {
        guard let soundURL = Bundle.main.url(forResource: inGameMusic, withExtension: mp3) else {
            return nil
        }
        do {
            let player = try AVAudioPlayer(contentsOf: soundURL)
            player.numberOfLoops = -1
            return player
        } catch {
            return nil
        }
    }()

}
