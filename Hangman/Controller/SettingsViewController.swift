//
//  SettingsViewController.swift
//  Hangman
//
//  Created by Shiva Skanthan on 13/10/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var soundEffectsSwitch: UISwitch!
    @IBOutlet weak var backgroundMusicSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Set switch based on locally saved settings
        soundEffectsSwitch.isOn = UserDefaults.standard.bool(forKey: soundEffectSetting)
        backgroundMusicSwitch.isOn = UserDefaults.standard.bool(forKey: backgroundMusicSetting)
    }
    
    @IBAction func soundEffectSwitchChange(_ sender: UISwitch) {
        //MARK:- Save sound effect setting
        UserDefaults.standard.set(sender.isOn, forKey: soundEffectSetting)
    }
    
    @IBAction func backgroundMusicSwitchChange(_ sender: UISwitch) {
        //MARK:- Save background music setting
        UserDefaults.standard.set(sender.isOn, forKey: backgroundMusicSetting)
        //MARK:- Send notification to WelcomeVC to turn off background music
        NotificationCenter.default.post(name: Notification.Name(rawValue: backgroundMusicSetting), object: nil)
    }
    
    
}
