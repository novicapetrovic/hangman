//
//  CategoryTableViewCell.swift
//  Hangman
//
//  Created by Shiva Skanthan on 20/10/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
