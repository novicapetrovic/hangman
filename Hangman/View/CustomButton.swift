//
//  CustomButton.swift
//  Hangman
//
//  Created by Shiva Skanthan on 20/10/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        //Set button corner radius
        layer.cornerRadius = 5.0
    }
}
