//
//  HangmanNavigationViewController.swift
//  Hangman
//
//  Created by Shiva Skanthan on 13/10/2018.
//  Copyright © 2018 Novica Petrovic. All rights reserved.
//

import UIKit

class HangmanNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
